//
//  LooperGui.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "LooperGui.h"

LooperGui::LooperGui(Looper& looper_) : looper (looper_)
{
    playButton.setButtonText ("Play");
    playButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    playButton.setColour(TextButton::buttonColourId, Colours::grey);
    playButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playButton);
    playButton.addListener (this);
    
    recordButton.setButtonText ("Record");
    recordButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    recordButton.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible (&recordButton);
    recordButton.addListener (this);
    
    saveButton.setButtonText ("Save");
    saveButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    saveButton.setColour(TextButton::buttonColourId, Colours::lightblue);
    saveButton.setColour(TextButton::buttonOnColourId, Colours::blue);
    addAndMakeVisible (&saveButton);
    saveButton.addListener (this);
    
    loadButton.setButtonText ("Load");
    loadButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    loadButton.setColour(TextButton::buttonColourId, Colours::lightgreen);
    loadButton.setColour(TextButton::buttonOnColourId, Colours::green);
    addAndMakeVisible (&loadButton);
    loadButton.addListener (this);
}

void LooperGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        looper.setPlayState (!looper.getPlayState());
        playButton.setToggleState (looper.getPlayState(), dontSendNotification);
    }
    else if (button == &recordButton)
    {
        looper.setRecordState (!looper.getRecordState());
        recordButton.setToggleState (looper.getRecordState(), dontSendNotification);
    }
    else if (button == &saveButton)
    {
        if (looper.getPlayState() == false)
            looper.save();
        else
            AlertWindow::showMessageBoxAsync (AlertWindow::WarningIcon, "Error", "Stop playback before trying to save.", "OK", this);
    }
    else if (button == &loadButton)
    {
        if (looper.getPlayState() == false)
            looper.load();
        else
            AlertWindow::showMessageBoxAsync (AlertWindow::WarningIcon, "Error", "Stop playback before trying to load.", "OK", this);
    }
}

void LooperGui::resized()
{
    playButton.setBounds (0, 0, getWidth() / 4, getHeight());
    recordButton.setBounds (playButton.getBounds().translated(getWidth() / 4, 0));
    saveButton.setBounds (recordButton.getBounds().translated(getWidth() / 4, 0));
    loadButton.setBounds (saveButton.getBounds().translated(getWidth() / 4, 0));
}